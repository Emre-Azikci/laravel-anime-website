<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnimemovieUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $animemovie = $this->route('animemovie');

        return [
            'moviename' =>  'required|string|min:5|max:45' . $animemovie->id,
            'summary' => 'required|string|max:200',
            'releasedate' => 'required|integer|numeric|digits_between:4,4'
        ];
    }
}
