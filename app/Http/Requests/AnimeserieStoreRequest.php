<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnimeserieStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'seriename' => 'required|string|unique:animeseries|max:45',
            'summary' => 'required|string|min:5|max:200',
            'releasedate' => 'required|integer|unique:animeseries|numeric|digits_between:4,4'
        ];
    }
}