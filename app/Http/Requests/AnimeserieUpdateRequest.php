<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnimeserieUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $animeserie = $this->route('animeserie');

        return [
            'seriename' => 'required|string|max:45|unique:animeseries,seriename,'.$animeserie->id,
            'summary' => 'required|string|min:5|max:200',
            'releasedate' => 'required|integer|numeric|digits_between:4,4'
        ];
    }
}
