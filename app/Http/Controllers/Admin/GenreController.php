<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GenreStoreRequest;
use App\Http\Requests\GenreUpdateRequest;
use App\Models\Genre;
use Illuminate\Http\Request;
use Throwable;

class GenreController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:index genre', ['only' => ['index']]);
        $this->middleware('permission:show genre', ['only' => ['show']]);
        $this->middleware('permission:create genre', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit genre', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete genre', ['only' => ['delete', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genres = Genre::all();

        return view('admin.genre.index', compact('genres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.genre.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenreStoreRequest $request)
    {
        $genre = new Genre();
        $genre->genrename = $request->genrename;
        $genre->save();

        return redirect()->route('genre.index')->with('status', 'Succesfully added new Genre!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function show(Genre $genre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function edit(Genre $genre)
    {
        return view('admin.genre.edit', compact('genre'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function update(GenreUpdateRequest $request, Genre $genre)
    {
        $genre->genrename = $request->genrename;
        $genre->save();

        return redirect()->route('genre.index')->with('status', 'Succesfully updated this Genre!');
    }

    public function delete(Genre $genre)
    {
        return view('admin.genre.delete', compact('genre'));   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Genre $genre)
    {
        try {
            $genre->delete();
        } catch (Throwable $error)
        {
            report($error);
            return redirect()->route('genre.index')->with('status-wrong', 'This genre is in use!
            Make sure it is not in use.');
        }

        return redirect()->route('genre.index')->with('status', 'This genre was deleted succesfully!');
    }
}
