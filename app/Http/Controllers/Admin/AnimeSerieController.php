<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AnimeserieStoreRequest;
use App\Http\Requests\AnimeserieUpdateRequest;
use App\Models\Animeserie;
use App\Models\Genre;
use App\Models\Season;
use Illuminate\Http\Request;

class AnimeserieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:index animeserie', ['only' => ['index']]);
        $this->middleware('permission:show animeserie', ['only' => ['show']]);
        $this->middleware('permission:create animeserie', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit animeserie', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete animeserie', ['only' => ['delete', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $animeseries = Animeserie::with('genre', 'season')->get();

        return view('admin.animeserie.index', compact('animeseries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $animeseries = Animeserie::with('genre')->get();
        $genres = Genre::with('animeserie')->get();
        $seasons = Season::with('animeserie')->get();

        return view('admin.animeserie.create', compact('animeseries', 'genres', 'seasons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnimeserieStoreRequest $request)
    {
        $animeserie = new Animeserie();
        $animeserie->seriename = $request->seriename;
        $animeserie->season_id = $request->season_id;
        $animeserie->summary = $request->summary;
        $animeserie->genre_id = $request->genre_id;
        $animeserie->releasedate = $request->releasedate;
        $animeserie->save();

        return redirect()->route('animeserie.index')->with('status', 'Succesfully added new Anime!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Animeserie  $animeserie
     * @return \Illuminate\Http\Response
     */
    public function show(Animeserie $animeserie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Animeserie  $animeserie
     * @return \Illuminate\Http\Response
     */
    public function edit(Animeserie $animeserie)
    {
        $genres = Genre::with('animeserie')->get();
        $seasons = Season::with('animeserie')->get();

        return view('admin.animeserie.edit', compact('animeserie', 'genres', 'seasons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Animeserie  $animeserie
     * @return \Illuminate\Http\Response
     */
    public function update(AnimeserieUpdateRequest $request, Animeserie $animeserie)
    {
        $animeserie->seriename = $request->seriename;
        $animeserie->season_id = $request->season_id;
        $animeserie->summary = $request->summary;
        $animeserie->genre_id = $request->genre_id;
        $animeserie->releasedate = $request->releasedate;
        $animeserie->save();

        return redirect()->route('animeserie.index')->with('status', 'Succesfully updated this Anime!');
    }

    public function delete(Animeserie $animeserie)
    {
        return view('admin.animeserie.delete', compact('animeserie'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Animeserie  $animeserie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Animeserie $animeserie)
    {
        $animeserie->delete();

        return redirect()->route('animeserie.index')->with('status', 'This Anime was deleted succesfully!');
    }
}
