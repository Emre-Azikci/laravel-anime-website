<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Animemovie;
use App\Http\Requests\AnimemovieStoreRequest;
use App\Http\Requests\AnimemovieUpdateRequest;
use App\Models\Genre;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AnimemovieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:index animemovie', ['only' => ['index']]);
        $this->middleware('permission:show animemovie', ['only' => ['show']]);
        $this->middleware('permission:create animemovie', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit animemovie', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete animemovie', ['only' => ['delete', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Animemovie $animemovies)
    {
        $animemovies = Animemovie::with('genre')->get();

        return view('admin.animemovie.index', compact('animemovies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $animemovies = Animemovie::with('genre')->get();
        $genres = Genre::with('animemovie')->get();

        return view('admin.animemovie.create', compact('animemovies', 'genres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnimemovieStoreRequest $request)
    {
        $animemovie = new Animemovie();
        $animemovie->moviename = $request->moviename;
        $animemovie->summary = $request->summary;
        $animemovie->releasedate = $request->releasedate;
        $animemovie->genre_id = $request->genre_id;
        $animemovie->updated_at = Carbon::now();
        $animemovie->save();

        return redirect()->route('animemovie.index')->with('status', 'Succesfully added a new Animemovie!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Animemovie  $animemovie
     * @return \Illuminate\Http\Response
     */
    public function show(Animemovie $animemovie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Animemovie  $animemovie
     * @return \Illuminate\Http\Response
     */
    public function edit(Animemovie $animemovie)
    {
        $genres = Genre::with('animemovie')->get();


        return view('admin.animemovie.edit', compact('animemovie', 'genres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Animemovie  $animemovie
     * @return \Illuminate\Http\Response
     */
    public function update(AnimemovieUpdateRequest $request, Animemovie $animemovie)
    {
        $animemovie->moviename = $request->moviename;
        $animemovie->summary = $request->summary;
        $animemovie->genre_id = $request->genre_id;
        $animemovie->releasedate = $request->releasedate;
        $animemovie->save();

        return redirect()->route('animemovie.index')->with('status', 'Succesfully updated this Animemovie!');
    }

    public function delete(Animemovie $animemovie)
    {
        return view('admin.animemovie.delete', compact('animemovie'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Animemovie  $animemovie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Animemovie $animemovie)
    {
        $animemovie->delete();

        return redirect()->route('animemovie.index')->with('status', 'This Animemovie was deleted succesfully!');
    }
}
