<?php

namespace App\Http\Controllers\Open;

use App\Http\Controllers\Controller;
use App\Models\Animeserie;
use Illuminate\Http\Request;

class AnimeserieOpenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $animeseries = Animeserie::with('genre')->get();

        return view('open.animeserie.index', compact('animeseries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Animeserie  $animeserie
     * @return \Illuminate\Http\Response
     */
    public function show(Animeserie $animeserie)
    {
        return view('open.animeserie.show', compact('animeserie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Animeserie  $animeserie
     * @return \Illuminate\Http\Response
     */
    public function edit(Animeserie $animeserie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Animeserie  $animeserie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Animeserie $animeserie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Animeserie  $animeserie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Animeserie $animeserie)
    {
        //
    }
}
