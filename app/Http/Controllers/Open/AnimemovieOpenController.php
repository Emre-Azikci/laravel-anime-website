<?php

namespace App\Http\Controllers\Open;

use App\Http\Controllers\Controller;
use App\Models\Animemovie;
use Illuminate\Http\Request;

class AnimemovieOpenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $animemovies = Animemovie::all();

        return view('open.animemovie.index', compact('animemovies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Animemovie  $animemovie
     * @return \Illuminate\Http\Response
     */
    public function show(Animemovie $animemovie)
    {
        return view('open.animemovie.show', compact('animemovie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Animemovie  $animemovie
     * @return \Illuminate\Http\Response
     */
    public function edit(Animemovie $animemovie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Animemovie  $animemovie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Animemovie $animemovie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Animemovie  $animemovie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Animemovie $animemovie)
    {
        //
    }
}
