<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Animemovie extends Model
{
    use HasFactory;

    protected $fillable = [
        'moviename',
        'summary',
        'releasedate'
    ];

    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }
}
