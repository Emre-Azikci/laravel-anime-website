<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Animeserie extends Model
{
    use HasFactory;

    protected $fillable = ['seriename', 'summary', 'releasedate'];

    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    public function season()
    {
        return $this->belongsTo(Season::class);
    }
}
