<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    protected $fillable = ['genrename'];

    public function question()
    {
        return $this->hasMany(Question::class);
    }

    public function animeserie()
    {
        return $this->hasMany(Animeserie::class);
    }

    public function animemovie()
    {
        return $this->hasMany(Animemovie::class);
    }
}
