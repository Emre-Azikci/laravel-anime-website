<?php

use Carbon\Carbon;
use App\Models\Animemovie;
use App\Models\Genre;
use \Pest\Laravel;

beforeEach(function () {
    $this->genre = Genre::factory()->create();
    $this->animemovie = Animemovie::factory()->create();
});

test('An animemovie has a genre', function () {
    expect($this->animemovie->genre)->toBeInstanceOf(Genre::class);
})->group('AnimemovieUnit');


test('An animemovie id is an int', function () {
    expect($this->animemovie->id)->toBeInt();
})->group('AnimemovieUnit');


test('An animemovie name is a string', function () {
    expect($this->animemovie->moviename)->toBeString();
})->group('AnimemovieUnit');


test('An animemovie summary is a string', function () {
    expect($this->animemovie->summary)->toBeString();
})->group('AnimemovieUnit');


test('An animemovie releasedate is numeric', function () {
    expect($this->animemovie->releasedate)->toBeNumeric();
})->group('AnimemovieUnit');


test('An animemovie created at is a datetime', function () {
    expect($this->animemovie->created_at)->toBeInstanceOf(Carbon::class);
})->group('AnimemovieUnit');


test('An animemovie updated at is a datetime', function () {
    expect($this->animemovie->updated_at)->toBeInstanceOf(Carbon::class);
})->group('AnimemovieUnit');