<?php

use App\Models\Animeserie;
use App\Models\Genre;
use App\Models\Season;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->season = Season::factory()->create();
    $this->animeserie = Animeserie::factory()->create();
});

test('admin can see animeserie create page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('animeserie.create'))
    ->assertViewIs('admin.animeserie.create')
    ->assertStatus(200);

})->group('AnimeserieCreate');

test('user cannot see animeserie create page', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('animeserie.create'))
    ->assertForbidden();

})->group('AnimeserieCreate');


test('guest cannot see animeserie create page', function () {
    $this->get(route('animeserie.create'))
        ->assertRedirect(route('login'));
        
})->group('AnimeserieCreate');
