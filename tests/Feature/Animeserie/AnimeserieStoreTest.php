<?php

use App\Models\Animeserie;
use App\Models\Genre;
use App\Models\Season;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->season = Season::factory()->create();
    $this->animeserie = Animeserie::factory()->create();
});

test('admin can create a animeserie', function () {
    $this->withoutExceptionHandling();
    $admin = User::find(2);
    $animeserie = Animeserie::factory()->make([
        'seriename' => 'One Piece',
        'season_id' => $this->season->id,
        'summary' => 'testsummary',
        'genre_id' => $this->genre->id,
        'releasedate' => '2018'
    ]);

    Laravel\be($admin)
        ->postJson(route('animeserie.store'), $animeserie->toArray())
        ->assertRedirect(route('animeserie.index'));

    $this->assertDatabaseHas('animeseries', [
        'seriename' => 'One Piece',
        'season_id' => 1,
        'summary' => 'testsummary',
        'genre_id' => 1,
        'releasedate' => '2018'
    ]);

})->group('AnimeserieStore');

test('user cannot create an animeserie', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->postJson(route('animeserie.store'))
    ->assertForbidden();

})->group('AnimeserieStore');


test('guest cannot create an animeserie', function () {
    $this->postJson(route('animeserie.store'))
        ->assertStatus(401);

})->group('AnimeserieStore');