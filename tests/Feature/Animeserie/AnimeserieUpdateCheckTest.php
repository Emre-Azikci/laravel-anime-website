<?php

use App\Models\Animeserie;
use App\Models\Genre;
use App\Models\Season;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->season = Season::factory()->create();
    $this->animeserie = Animeserie::factory()->create();
});

function patchAnimeserie($overridesAnimeserie = [])
{
    $animeserie = Animeserie::find(1)->make($overridesAnimeserie);
    return Laravel\patchJson(route('animeserie.update', ['animeserie' => 1]), $animeserie->toArray());
}

test('a serie requires a name', function () {
    $admin = User::find(2);
    Laravel\be($admin);
        patchAnimeserie(['seriename' => null])
        ->assertStatus(422);

})->group('AnimeserieUpdateCheck');

test('a summary requires some text', function () {
    $admin = User::find(2);
    Laravel\be($admin);
        patchAnimeserie(['summary' => null,])
        ->assertStatus(422);

})->group('AnimeserieUpdateCheck');

test('a releasedate requires a date', function () {
    $admin = User::find(2);
    Laravel\be($admin);
        patchAnimeserie(['releasedate' => null,])
        ->assertStatus(422);

})->group('AnimeserieUpdateCheck');


test('a seriename must be unique', function () {
    $admin = User::find(2);
    $this->animeserie = Animeserie::factory()->create(['seriename' => 'Serie1']);
    Laravel\be($admin);
        patchAnimeserie(['seriename' => 'Serie1'])
        ->assertStatus(422);

})->group('AnimeserieUpdateCheck');

test('a seriename can be max 45 character', function () {
    $admin = User::find(2);
    Laravel\be($admin);
        patchAnimeserie(['seriename' => '1234567890123456789012345678901234567890123456'])
        ->assertStatus(422);

})->group('AnimeserieUpdateCheck');