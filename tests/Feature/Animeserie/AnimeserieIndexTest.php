<?php

use App\Models\Animeserie;
use App\Models\Genre;
use App\Models\Season;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->season = Season::factory()->create();
    $this->animeserie = Animeserie::factory()->create();
});

test('admin can see animeserie index page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('animeserie.index'))
    ->assertSee($this->animeserie->seriename)
    ->assertSee($this->animeserie->season->seasonname)
    ->assertSee($this->animeserie->summary)
    ->assertSee($this->animeserie->genre->genrename)
    ->assertSee($this->animeserie->releasedate)
    ->assertStatus(200);

})->group('AnimeserieIndex');


test('user cannot see animeserie index page', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('animeserie.index'))
    ->assertForbidden();

})->group('AnimeserieIndex');


test('guest cannot see animeserie index page', function () {
    $this->get(route('animeserie.index'))
        ->assertRedirect(route('login'));
       
})->group('AnimeserieIndex');