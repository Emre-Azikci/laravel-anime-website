<?php

namespace Tests\Feature\Genre;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Models\Genre;
use App\Models\Season;
use App\Models\Animeserie;
use App\Models\User;


class AnimeserieStoreCheckTest extends TestCase
{
    use DatabaseMigrations;

    public function setup(): void
    {
        parent::setup();

        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');
        $this->genre = Genre::factory()->create();
        $this->season = Season::factory()->create();
        $this->animeserie = Animeserie::factory()->create();
    }

    public function postAnimeserie($overrides = [])
    {
        $animeserie = Animeserie::factory()->make($overrides);
        $genre = Genre::factory()->make();
        $season = Season::factory()->make();
        
        return $this->postJson(route('animeserie.store'), array_merge($animeserie->toArray(), $genre->toArray(), $season->toArray()));
    }

    /** @test 
     *  @group AnimeserieStoreCheck
    */
    function a_animeserie_requires_a_name()
    {
        $admin = User::find(2);
        $this->actingAs($admin);
        $this->postAnimeserie(['seriename' => null])->assertStatus(422);
    }

    /** @test 
     *  @group AnimeserieStoreCheck
    */
    function a_summary_requires_some_text()
    {
        $admin = User::find(2);
        $this->actingAs($admin);
        $this->postAnimeserie(['summary' => null])->assertStatus(422);
    }

    /** @test 
     *  @group AnimeserieStoreCheck
    */
    function a_releasedate_requires_a_date()
    {
        $admin = User::find(2);
        $this->actingAs($admin);
        $this->postAnimeserie(['releasedate' => null])->assertStatus(422);
    }

    /** @test 
     *  @group AnimeserieStoreCheck
    */
    function a_animeserie_must_be_unique()
    {
        $admin = User::find(2);
        $animeserie = Animeserie::find(1);
        $this->actingAs($admin);
        $this->postAnimeserie(['seriename' => $animeserie->seriename])->assertStatus(422);
    }

    /** @test 
     *  @group AnimeserieStoreCheck
    */
    function a_animeserie_can_be_max_45_characters()
    {
        $admin = User::find(2);
        $this->actingAs($admin);
        $this->postAnimeserie(['seriename' => '1234567890123456789012345678901234567890123456'])->assertStatus(422);
    }
}