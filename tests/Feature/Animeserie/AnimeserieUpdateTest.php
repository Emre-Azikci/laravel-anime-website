<?php

use App\Models\Animeserie;
use App\Models\Genre;
use App\Models\Season;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->season = Season::factory()->create();
    $this->animeserie = Animeserie::factory()->create();
});

test('admin can update a animeserie in', function () {
    $admin = User::find(2);
    $newgenre = Genre::factory()->create(['genrename' => 'TestGenre']);
    $newseason = Season::factory()->create(['seasonname' => 'TestSeason']);
    Laravel\be($admin)
        ->patchJson(route('animeserie.update', ['animeserie' => $this->animeserie->id]), 
        ['seriename' => 'A seriename',
            'season_id' => $newseason->id, 
            'summary' => 'Blablabla',
            'genre_id' => $newgenre->id,
            'releasedate' => '2018']
    );

    $this->animeserie = $this->animeserie->fresh();

    $this->get(route('animeserie.index'))
        ->assertSee('A seriename')
        ->assertSee($newseason->id)
        ->assertSee('Blablabla')
        ->assertSee($newgenre->id)
        ->assertSee('2018');
        

    $this->get(route('animeserie.index'))
    ->assertSee($this->animeserie->seriename)
    ->assertSee($this->season->id)
    ->assertSee($this->animeserie->summary)
    ->assertSee($this->genre->id)
    ->assertSee($this->animeserie->releasedate);

})->group('AnimeserieUpdate');

test('user cannot update an animeserie', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->patchJson(route('animeserie.update', ['animeserie' => $this->animeserie->id]))
    ->assertForbidden();

})->group('AnimeserieUpdate');


test('guest cannot update an animeserie', function () {
    $this->patchJson(route('animeserie.update', ['animeserie' => $this->animeserie->id]))
        ->assertStatus(401);

})->group('AnimeserieUpdate');
