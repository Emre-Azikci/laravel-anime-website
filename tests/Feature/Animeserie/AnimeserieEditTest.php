<?php

use App\Models\Animeserie;
use App\Models\Genre;
use App\Models\Season;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->season = Season::factory()->create();
    $this->animeserie = Animeserie::factory()->create();
});

test('admin can see animeserie page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('animeserie.edit', ['animeserie' => $this->animeserie->id]))
    ->assertViewIs('admin.animeserie.edit')
    ->assertSee($this->animeserie->seriename)
    ->assertSee($this->season->seasonname)
    ->assertSee($this->animeserie->summary)
    ->assertSee($this->genre->genrename)
    ->assertSee($this->animeserie ->releasedate)
    ->assertStatus(200);


})->group('AnimeserieEdit');

test('user cannot see animeserie page', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('animeserie.edit', ['animeserie' => $this->animeserie->id]))
    ->assertForbidden();

})->group('AnimeserieEdit');


test('guest cannot see animeserie page', function () {
    $this->get(route('animeserie.edit', ['animeserie' => $this->animeserie->id]))
        ->assertRedirect(route('login'));
        
})->group('AnimeserieEdit');