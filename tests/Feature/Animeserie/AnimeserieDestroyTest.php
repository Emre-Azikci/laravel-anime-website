<?php

use App\Models\Animeserie;
use App\Models\Genre;
use App\Models\Season;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->season = Season::factory()->create();
    $this->animeserie = Animeserie::factory()->create();
});

test('admin can delete a serie', function () {
    $admin = User::find(2);
    Laravel\be($admin);
    $this->json('DELETE', route('animeserie.destroy', ['animeserie' => $this->animeserie->id]));
    $this->assertDatabaseMissing('animeseries', ['id' => $this->animeserie->id]);
    // $this->assertDatabaseMissing('genres', ['id' => $this->genre->id]);
    // $this->assertDatabaseMissing('seasons', ['id' => $this->season->id]);

})->group('AnimeserieDestroy');

test('user cannot delete a serie', function () {
    $admin = User::find(1);
    Laravel\be($admin);
    $this->json('DELETE', route('animeserie.destroy', ['animeserie' => $this->animeserie->id]))
    ->assertForbidden();

})->group('AnimeserieDestroy');

test('guest cannot delete a serie', function () {
    $this->json('DELETE', route('animeserie.destroy', ['animeserie' => $this->animeserie->id]))
    ->assertStatus(401);

})->group('AnimeserieDestroy');