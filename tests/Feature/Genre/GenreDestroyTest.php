<?php

use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
});

test('admin can delete a genre', function () {
    $admin = User::find(2);
    Laravel\be($admin);
    $this->json('DELETE', route('genre.destroy', ['genre' => $this->genre->id]));
    $this->assertDatabaseMissing('genres', ['id' => $this->genre->id]);

})->group('GenreDestroy');

test('user cannot delete a genre', function () {
    $admin = User::find(1);
    Laravel\be($admin);
    $this->json('DELETE', route('genre.destroy', ['genre' => $this->genre->id]))
    ->assertForbidden();

})->group('GenreDestroy');

test('guest cannot delete a genre', function () {
    $this->json('DELETE', route('genre.destroy', ['genre' => $this->genre->id]))
    ->assertStatus(401);

})->group('GenreDestroy');