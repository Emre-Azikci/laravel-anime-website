<?php

use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
});

test('admin can see genre create page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('genre.create'))
    ->assertViewIs('admin.genre.create')
    ->assertStatus(200);

})->group('GenreCreate');

test('user cannot see genre create page', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('genre.create'))
    ->assertForbidden();

})->group('GenreCreate');


test('guest cannot see genre create page', function () {
    $this->get(route('genre.create'))
        ->assertRedirect(route('login'));
        
})->group('GenreCreate');
