<?php

use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
});

function patchGenre()
{
    $genre = Genre::find(1)->make();
    return Laravel\patchJson(route('genre.update', ['genre' => 1]), $genre->toArray());
}

test('a genre requires a name', function () {
    $admin = User::find(2);
    Laravel\be($admin);
        patchGenre(['genrename' => null])
        ->assertStatus(422);


})->group('GenreUpdateCheck');

test('a genrename must be unique', function () {
    $admin = User::find(2);
    $genre = Genre::factory()->create(['genrename' => 'Genre1']);
    Laravel\be($admin);
        patchGenre(['genrename' => 'Genre1'])
        ->assertStatus(422);


})->group('GenreUpdateCheck');

test('a genrename can be max 45 character', function () {
    $admin = User::find(2);
    Laravel\be($admin);
        patchGenre(['genrename' => '1234567890123456789012345678901234567890123456'])
        ->assertStatus(422);


})->group('GenreUpdateCheck');