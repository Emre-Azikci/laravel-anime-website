<?php

use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
});

test('admin can update a genre', function () {
    $admin = User::find(2);
    Laravel\be($admin)
        ->patchJson(route('genre.update', ['genre' => $this->genre->id]), 
        ['genrename' => 'A genrename']
    );

    $this->genre = $this->genre->fresh();

    $this->get(route('genre.index'))
        ->assertSee('A genrename');

    $this->get(route('genre.index'))
    ->assertSee($this->genre->genrename);

})->group('GenreUpdate');

test('user cannot update an genre', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->patchJson(route('genre.update', ['genre' => $this->genre->id]))
    ->assertForbidden();

})->group('GenreUpdate');


test('guest cannot update an genre', function () {
    $this->patchJson(route('genre.update', ['genre' => $this->genre->id]))
        ->assertStatus(401);

})->group('GenreUpdate');