<?php

namespace Tests\Feature\Genre;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Models\Genre;
use App\Models\User;


class GenreStoreCheckTest extends TestCase
{
    use DatabaseMigrations;

    public function setup(): void
    {
        parent::setup();

        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');
        $this->genre = Genre::factory()->create();
    }

    public function postGenre($overrides = [])
    {
        $genre = Genre::factory()->make($overrides);
        
        return $this->postJson(route('genre.store'), $genre->toArray());
    }

    /** @test 
     *  @group GenreStoreCheck
    */
    function a_genre_requires_a_name()
    {
        $admin = User::find(2);
        $this->actingAs($admin);
        $this->postGenre(['genrename' => null])->assertStatus(422);
    }

    /** @test 
     *  @group GenreStoreCheck
    */
    function a_genrename_must_be_unique()
    {
        $admin = User::find(2);
        $genre = Genre::find(1);
        $this->actingAs($admin);
        $this->postGenre(['genrename' => $genre->genrename])->assertStatus(422);
    }

    /** @test 
     *  @group GenreStoreCheck
    */
    function a_genrename_can_be_max_45_characters()
    {
        $admin = User::find(2);
        $this->actingAs($admin);
        $this->postGenre(['genrename' => '1234567890123456789012345678901234567890123456'])->assertStatus(422);
    }
}