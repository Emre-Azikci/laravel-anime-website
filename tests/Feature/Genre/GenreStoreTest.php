<?php

use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
});

test('admin can create a genre', function () {
    $admin = User::find(2);
    $genre = Genre::factory()->make();

    Laravel\be($admin)
    ->postJson(route('genre.store'), $genre->toArray())
    ->assertRedirect(route('genre.index'));

    $this->assertDatabaseHas('genres', [
        'genrename' => $genre->genrename
    ]);

})->group('GenreStore');

test('user cannot create an genre', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->postJson(route('genre.store'))
    ->assertForbidden();

})->group('GenreStore');


test('guest cannot create an genre', function () {
    $this->postJson(route('genre.store'))
        ->assertStatus(401);

})->group('GenreStore');