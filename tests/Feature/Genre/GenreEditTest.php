<?php

use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
});

test('admin can see genre edit page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('genre.edit', ['genre' => $this->genre->id]))
    ->assertViewIs('admin.genre.edit')
    ->assertSee($this->genre->genrename)
    ->assertStatus(200);


})->group('GenreEdit');

test('user cannot see genre edit page', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('genre.edit', ['genre' => $this->genre->id]))
    ->assertForbidden();

})->group('GenreEdit');


test('guest cannot see genre edit page', function () {
    $this->get(route('genre.edit', ['genre' => $this->genre->id]))
        ->assertRedirect(route('login'));
        
})->group('GenreEdit');