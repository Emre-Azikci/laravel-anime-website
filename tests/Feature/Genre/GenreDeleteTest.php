<?php

use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
});

test('admin can see genre delete page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('genre.delete', ['genre' => $this->genre->id]))
    ->assertViewIs('admin.genre.delete')
    ->assertSee($this->genre->genrename)
    ->assertStatus(200);

})->group('GenreDelete');

test('user cannot see genre delete page', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('genre.delete', ['genre' => $this->genre->id]))
    ->assertForbidden();

})->group('GenreDelete');

test('guest cannot see genre delete page', function () {
    $this->get(route('genre.delete' , ['genre' => $this->genre->id]))
        ->assertRedirect(route('login'));
        
})->group('GenreDelete');