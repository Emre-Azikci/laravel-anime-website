<?php

use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
});

test('admin can see genre index page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('genre.index'))
    ->assertSee($this->genre->genrename)
    ->assertStatus(200);

})->group('GenreIndex');

test('user cannot see genre index page', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('genre.index'))
    ->assertForbidden();

})->group('GenreIndex');

test('guest cannot see genre index page', function () {
    $this->get(route('genre.index'))
    ->assertRedirect(route('login'));
    
})->group('GenreIndex');