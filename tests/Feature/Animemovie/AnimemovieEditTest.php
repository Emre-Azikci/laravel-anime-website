<?php

use App\Models\User;
use App\Models\Animemovie;
use App\Models\Genre;
use pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->animemovie = Animemovie::factory()->create();
});

test('guest can not see the animemovie edit page', function () {
    $this->get(route('animemovie.edit', ['animemovie' => $this->animemovie->id]))
        ->assertRedirect(route('login'));
})->group('AnimemovieEdit');

test('user can not see the animemovie edit page', function () {
    $user = User::find(1);
    Laravel\be($user)
        ->get(route('animemovie.edit', ['animemovie' => $this->animemovie->id]))
        ->assertForbidden();
})->group('AnimemovieEdit');

test('admin can see the animemovie edit page', function () {
    $this->withoutExceptionHandling();
    $admin = User::find(2);
    Laravel\be($admin)
        ->get(route('animemovie.edit', ['animemovie' => $this->animemovie->id]))
        ->assertViewIs('admin.animemovie.edit')
        ->assertSee($this->animemovie->moviename)
        ->assertSee($this->animemovie->summary)
        ->assertSee($this->animemovie->releasedate)
        ->assertSee($this->animemovie->genre->genrename)
        ->assertStatus(200);
})->group('AnimemovieEdit', 'Animemovie');
