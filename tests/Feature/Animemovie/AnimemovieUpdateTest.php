<?php

use App\Models\User;
use App\Models\Genre;
use App\Models\Animemovie;
use Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->animemovie = Animemovie::factory()->create();
});

test('guest can not update an animemovie', function () {
    $animemovie = Animemovie::find(1);
    $this->patchJson(
        route('animemovie.update', ['animemovie' => $this->animemovie->id]),
        $animemovie->toArray()
    )
        ->assertStatus(401);
})->group('AnimemovieUpdate');

test('user can not update an animemovie', function () {
    $user = User::find(1);
    $animemovie = Animemovie::find(1);

    Laravel\be($user);
    $this->patchJson(
        route('animemovie.update', ['animemovie' => $this->animemovie->id]),
        $animemovie->toArray()
    )
        ->assertForbidden();
})->group('AnimemovieUpdate');

test('admin can update an animemovie', function () {
    $admin = User::find(2);
    $newanimemovie = Animemovie::factory()->create(['moviename' => 'Testmovie']);

    Laravel\be($admin);
    $this->patchJson(
        route('animemovie.update', ['animemovie' => $this->animemovie->id]),
        [
            'moviename' => 'A movie name',
            'summary' => 'This is a test summary',
            'genre_id' => 1,
            'releasedate' => 1996
        ]
    );

    $this->animemovie = $this->animemovie->fresh();

    $this->get(route('animemovie.index'))
        ->assertSee('A movie name')
        ->assertSee('This is a test summary')
        ->assertSee($newanimemovie->genre_id)
        ->assertSee(1996)
        ->assertSee($newanimemovie->moviename);
})->group('AnimemovieUpdate', 'Animemovie');
