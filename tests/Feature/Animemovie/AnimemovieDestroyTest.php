<?php

use App\Models\User;
use App\Models\Animemovie;
use App\Models\Genre;
use Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->animemovie = Animemovie::factory()->create();
});

test('guest can not destroy an animemovie', function () {
    $this->json('DELETE', route('animemovie.destroy', ['animemovie' => $this->animemovie->id]))
        ->assertStatus(401);
})->group('AnimemovieDestroy');

test('user can not destroy an animemovie', function () {
    $user = User::find(1);
    Laravel\be($user);
    $this->json('DELETE', route('animemovie.destroy', ['animemovie' => $this->animemovie->id]))
        ->assertForbidden();
})->group('AnimemovieDestroy');

test('admin can destroy an animemovie', function () {
    $admin = User::find(2);
    Laravel\be($admin);
    $this->json('DELETE', route('animemovie.destroy', ['animemovie' => $this->animemovie->id]));
    $this->assertDatabaseMissing('animemovies', ['id' => $this->animemovie->id]);
})->group('AnimemovieDestroy', 'Animemovie');
