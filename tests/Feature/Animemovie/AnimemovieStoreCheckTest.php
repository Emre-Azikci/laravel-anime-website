<?php

use App\Models\User;
use App\Models\Genre;
use App\Models\Animemovie;
use Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->animemovie = Animemovie::factory()->create();
});

function postAnimemovie($overrides = [])
{
    $genre = Genre::factory()->make();
    $animemovie = Animemovie::factory()->make($overrides);
    return Laravel\postJson(route('animemovie.store'), $animemovie->toArray());
}

test('an animemovie requires a moviename', function () {
    $admin = User::find(2);

    Laravel\be($admin);
    postAnimemovie(['moviename' => null])
        ->assertStatus(422);
})->group('AnimemovieStoreCheck');

test('an animemovie name can be max 45 characters', function () {
    $admin = User::find(2);

    Laravel\be($admin);
    postAnimemovie(['moviename' => '1234567890123456789012345678901234567890123456'])
        ->assertStatus(422);
})->group('AnimemovieStoreCheck');

test('an animemovie name must be unique', function () {
    $admin = User::find(2);
    $animemovie = Animemovie::find(1);

    Laravel\be($admin);
    postAnimemovie(['moviename' => $animemovie->moviename])
        ->assertStatus(422);
})->group('AnimemovieStoreCheck');

test('an animemovie requires a summary', function () {
    $admin = User::find(2);

    Laravel\be($admin);
    postAnimemovie(['summary' => null])
        ->assertStatus(422);
})->group('AnimemovieStoreCheck');

test('an animemovie requires a releasedate', function () {
    $admin = User::find(2);

    Laravel\be($admin);
    postAnimemovie(['releasedate' => null])
        ->assertStatus(422);
})->group('AnimemovieStoreCheck', 'Animemovie');
