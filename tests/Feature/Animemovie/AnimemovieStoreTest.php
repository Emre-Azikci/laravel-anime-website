<?php

use App\Models\User;
use App\Models\Animemovie;
use App\Models\Genre;
use Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->animemovie = Animemovie::factory()->create();
});

test('a guest can not store an animemovie in the database', function () {
    $this->postJson(route('animemovie.store'))
        ->assertStatus(401);
})->group('AnimemovieStore');

test('a user can not store an animemovie in the database', function () {
    $user = User::find(1);
    Laravel\be($user)
        ->postJson(route('animemovie.store'))
        ->assertForbidden();
})->group('AnimemovieStore');

test('an admin can store an animemovie in the database', function () {
    $this->withoutExceptionHandling();
    $admin = User::find(2);
    $animemovie = Animemovie::factory()->make([
        'moviename' => 'Jujutsu kaisen',
        'summary' => 'Testing this',
        'genre_id' => $this->genre->id,
        'releasedate' => '1996'
        ]);

    Laravel\be($admin)
        ->postJson(route('animemovie.store'),  $animemovie->toArray())
        ->assertRedirect(route('animemovie.index'));


    $this->assertDatabaseHas('animemovies', [
        'moviename' => 'Jujutsu kaisen',
        'summary' => 'Testing this',
        'genre_id' => 1,
        'releasedate' => '1996'
    ]);
})->group('AnimemovieStore', 'Animemovie');
