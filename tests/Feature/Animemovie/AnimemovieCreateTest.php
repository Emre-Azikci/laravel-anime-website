<?php

use App\Models\User;
use App\Models\Animemovie;
use App\Models\Genre;
use Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->animemovie = Animemovie::factory()->create();
});

test('guest can not see the animemovie create page', function () {
    $this->get(route('animemovie.create'))
        ->assertRedirect(route('login'));
})->group('AnimemovieCreate');

test('user can not see the animemovie create page', function () {
    $user = User::find(1);
    Laravel\be($user)
        ->get(route('animemovie.create'))
        ->assertForbidden();
})->group('AnimemovieCreate');

test('admin can see the animemovie create page', function () {
    $this->withoutExceptionHandling();
    $admin = User::find(2);
    Laravel\be($admin)
        ->get(route('animemovie.create'))
        ->assertViewIs('admin.animemovie.create')
        ->assertStatus(200);
})->group('AnimemovieCreate', 'Animemovie');
