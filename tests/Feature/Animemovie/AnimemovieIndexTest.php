<?php

use App\Models\User;
use App\Models\Animemovie;
use App\Models\Genre;
use \Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->animemovie = Animemovie::factory()->create();
});

test('guest cannot see animemovie index page', function () {
    $this->get(route('animemovie.index'))
        ->assertRedirect(route('login'));
})->group('AnimemovieIndex');

test('user cannot see animemovie index page', function () {
    $user = User::find(1);
    Laravel\be($user)
        ->get(route('animemovie.index'))
        ->assertForbidden();
})->group('AnimemovieIndex');

test('admin can view the animemovie index page', function () {
    $this->withoutExceptionHandling();
    $admin = User::find(2);
    Laravel\be($admin)
        ->get(route('animemovie.index'))
        ->assertViewIs('admin.animemovie.index')
        ->assertSee($this->animemovie->moviename)
        ->assertSee($this->animemovie->summary)
        ->assertSee($this->animemovie->genre->genrename)
        ->assertStatus(200);
})->group('AnimemovieIndex', 'Animemovie');
