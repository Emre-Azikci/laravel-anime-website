<?php

use App\Models\User;
use App\Models\Genre;
use App\Models\Animemovie;
use Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->animemovie = Animemovie::factory()->create();
});

function patchAnimemovie($overridesAnimemovie = [])
{
    $animemovie = Animemovie::find(1)->make($overridesAnimemovie);
    return Laravel\patchJson(
        route('animemovie.update', ['animemovie' => 1]),
        $animemovie->toArray()
    );
}

test('an animemovie requires a moviename', function () {
    $admin = User::find(2);
    Laravel\be($admin);
    patchAnimemovie(['moviename' => null])
        ->assertStatus(422);
})->group('AnimemovieUpdateCheck');

test('an animemovie name can be max 45 characters', function () {
    $admin = User::find(2);
    Laravel\be($admin);
    patchAnimemovie(['moviename' => '1234567890123456789012345678901234567890123456'])
        ->assertStatus(422);
})->group('AnimemovieUpdateCheck');

test('an animemovie name must be unique', function () {
    $admin = User::find(2);
    $animemovie = Animemovie::factory()->create(['moviename' => 'Animemovie1']);
    Laravel\be($admin);
    patchAnimemovie(['moviename' => 'Animemovie1'])
        ->assertStatus(422);
})->group('AnimemovieUpdateCheck');

test('an animemovie requires a summary', function () {
    $admin = User::find(2);
    Laravel\be($admin);
    patchAnimemovie(['summary' => null])
        ->assertStatus(422);
})->group('AnimemovieUpdateCheck');

test('an animemovie requires a releasedate', function () {
    $admin = User::find(2);
    Laravel\be($admin);
    patchAnimemovie(['releasedate' => null])
        ->assertStatus(422);
})->group('AnimemovieUpdateCheck', 'Animemovie');
