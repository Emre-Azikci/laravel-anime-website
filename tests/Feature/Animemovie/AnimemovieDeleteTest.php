<?php

use App\Models\User;
use App\Models\Animemovie;
use App\Models\Genre;
use Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->animemovie = Animemovie::factory()->create();
});

test('guest can not see the animemovie delete page', function () {
    $this->get(route('animemovie.delete', ['animemovie' => $this->animemovie->id]))
        ->assertRedirect(route('login'));
})->group('AnimemovieDelete');

test('user can not see the animemovie delete page', function () {
    $user = User::find(1);
    Laravel\be($user)
        ->get(route('animemovie.delete', ['animemovie' => $this->animemovie->id]))
        ->assertForbidden();
})->group('AnimemovieDelete');

test('admin can see the animemovie delete page', function () {
    $this->withoutExceptionHandling();
    $admin = User::find(2);
    Laravel\be($admin)
        ->get(route('animemovie.delete', ['animemovie' => $this->animemovie->id]))
        ->assertViewIs('admin.animemovie.delete')
        ->assertSee($this->animemovie->moviename)
        ->assertStatus(200);
})->group('AnimemovieDelete', 'Animemovie');
