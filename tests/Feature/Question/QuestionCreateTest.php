<?php

use App\Models\Genre;
use App\Models\Question;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->user = User::factory()->create();
    $this->genre = Genre::factory()->create();
    $this->question = Question::factory()->create();
});

test('admin can see question create page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('question.create'))
    ->assertViewIs('admin.question.create')
    ->assertStatus(200);

})->group('QuestionCreate');

test('user can see question create page)', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('question.create'))
    ->assertStatus(200);

})->group('QuestionCreate');


test('guest cannot see question create page', function () {
    $this->get(route('question.create'))
        ->assertRedirect(route('login'));
        
})->group('QuestionCreate');