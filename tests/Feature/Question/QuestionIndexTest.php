<?php

use App\Models\Question;
use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->user = User::factory()->create();
    $this->genre = Genre::factory()->create();
    $this->question = Question::factory()->create();
});

test('admin can see question index page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('question.index'))
    ->assertSee($this->question->user->name)
    ->assertSee($this->question->animetitle)
    ->assertSee($this->question->genre->genrename)
    ->assertStatus(200);

})->group('QuestionIndex');


test('user cannot see question index page', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('question.index'))
    ->assertForbidden();

})->group('QuestionIndex');


test('guest cannot see question index page', function () {
    $this->get(route('question.index'))
        ->assertRedirect(route('login'));
       
})->group('QuestionIndex');