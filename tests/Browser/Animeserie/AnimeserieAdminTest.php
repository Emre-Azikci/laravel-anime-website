<?php

namespace Tests\Browser\Animeserie;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Models\User;
use App\Models\Genre;
use App\Models\Season;
use App\Models\Animeserie;


class AnimeserieAdminTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function admin_can_create_an_animeserie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->loginAs(User::find(2))
                    ->visit('/admin/animeserie')
                    ->assertSee('Add +')
                    ->clickLink('Add +')
                    ->type('seriename', 'Jujuju')
                    ->select('season_id')
                    ->type('summary', 'Testingthis')
                    ->select('genre_id')
                    ->type('releasedate', '1998')
                    ->press('Add')
                    ->assertSee('Succesfully added new Anime!');
        });
    }

    /** @test */
    public function admin_can_edit_an_animeserie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->loginAs(User::find(2))
                    ->visit('/admin/animeserie')
                    ->assertSee('Edit')
                    ->clickLink('Edit')
                    ->type('seriename', 'testname')
                    ->type('summary', 'testsummary')
                    ->type('releasedate', '1990')
                    ->select('genre_id')
                    ->press('Edit')
                    ->assertSee('Succesfully updated this Anime!');
        });
    }

    /** @test */
    public function admin_can_delete_an_animeserie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->loginAs(User::find(2))
                    ->visit('/admin/animeserie')
                    ->assertSee('Delete')
                    ->clickLink('Delete')
                    ->assertSee('Delete')
                    ->press('Delete')
                    ->assertSee('This Anime was deleted succesfully!')
                    ->assertDontSee($animeserie->seriename);
        });
    }
}
