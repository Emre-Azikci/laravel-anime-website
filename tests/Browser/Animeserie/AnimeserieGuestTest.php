<?php

namespace Tests\Browser\Animeserie;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Models\User;
use App\Models\Genre;
use App\Models\Season;
use App\Models\Animeserie;

class AnimeserieGuestTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function can_view_animeserie_index()
    {
        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->visit('/series')
                    ->assertSee('Populair Series')
                    ->assertSee($animeserie->seriename)
                    ->assertSee('Episode 1008');
        });
    }

    /** @test */
    public function can_view_animeserie_show()
    {
        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->visit('/animeserie/1/show')
                    ->assertSee('Anime Info')
                    ->assertSee($animeserie->seriename)
                    ->assertSee($animeserie->season->seasonname)
                    ->assertSee($animeserie->summary)
                    ->assertSee($animeserie->genre->genrename)
                    ->assertSee($animeserie->releasedate);
        });
    }

    /** @test */
    public function guest_can_not_create_an_animeserie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->visit('/admin/animeserie/create')
                    ->assertPathIs('/login')
                    ->assertSee('Login');
        });
    }

    /** @test */
    public function guest_can_not_edit_an_animeserie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->visit('/admin/animeserie/1/edit')
                    ->assertPathIs('/login')
                    ->assertSee('Login');
        });
    }
   
    /** @test */
    public function guest_can_not_delete_an_animeserie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->visit('/admin/animeserie/1/delete')
                    ->assertPathIs('/login')
                    ->assertSee('Login');
        });
    }
}
