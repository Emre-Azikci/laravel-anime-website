<?php

namespace Tests\Browser\Animeserie;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Models\User;
use App\Models\Genre;
use App\Models\Season;
use App\Models\Animeserie;

class AnimeserieUserTest extends DuskTestCase
{
    use DatabaseMigrations;

    
    /** @test */
    public function user_can_not_create_an_animeserie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->loginAs(User::find(1))
                    ->visit('/admin/animeserie/create')
                    ->assertSee('USER DOES NOT HAVE THE RIGHT PERMISSIONS.');
        });
    }

    /** @test */
    public function user_can_not_edit_an_animeserie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->loginAs(User::find(1))
                    ->visit('/admin/animeserie/1/edit')
                    ->assertSee('USER DOES NOT HAVE THE RIGHT PERMISSIONS.');
        });
    }
   
    /** @test */
    public function user_can_not_delete_an_animeserie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animeserie = Animeserie::factory([
            'season_id' => Season::factory()->create()->id,
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animeserie) {
            $browser->loginAs(User::find(1))
                    ->visit('/admin/animeserie/1/delete')
                    ->assertSee('USER DOES NOT HAVE THE RIGHT PERMISSIONS.');
        });
    }
}
