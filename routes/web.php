<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\GenreController;
use App\Http\Controllers\Admin\QuestionController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\AnimemovieController;
use App\Http\Controllers\Admin\AnimeserieController;
use App\Http\Controllers\Open\AnimemovieOpenController;
use App\Http\Controllers\Open\AnimeserieOpenController;
use App\Http\Controllers\Admin\SeasonController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AnimeserieOpenController::class, 'index'])
    ->name('animeserie.index');

Route::get('/series', [AnimeserieOpenController::class, 'index'])
    ->name('animeserie.index');

Route::get('/animeserie/{animeserie}/show', [AnimeserieOpenController::class, 'show'])
    ->name('open.animeserie.show');
Route::resource('/open/animeserie', AnimeserieOpenController::class);

Route::get('/animeserie', [AnimeserieOpenController::class, 'index'])
    ->name('open.animeserie.index');
Route::resource('/open/animeserie', AnimeserieOpenController::class);


Route::get('/animemovie', [AnimemovieOpenController::class, 'index'])
    ->name('open.animemovie.index');
Route::resource('/open/animemovie', AnimemovieOpenController::class);

Route::get('/animemovie/{animemovie}', [AnimemovieOpenController::class, 'show'])
    ->name('open.animemovie.show');
Route::resource('/open/animemovie  ', AnimemovieOpenController::class);



// Auth
Route::group(['middleware' => ['role:user|admin']], function () {
    Route::get('admin/user/{user}/delete', [UserController::class, 'delete'])
        ->name('user.delete');
    Route::resource('/admin/user', UserController::class);

    Route::get('admin/genre/{genre}/delete', [GenreController::class, 'delete'])
        ->name('genre.delete');
    Route::resource('/admin/genre', GenreController::class);

    Route::get('admin/seasons/{season}/delete', [SeasonController::class, 'delete'])
        ->name('seasons.delete');
    Route::resource('/admin/seasons', SeasonController::class);

    Route::get('admin/question/{question}/delete', [QuestionController::class, 'delete'])
        ->name('question.delete');
    Route::resource('/admin/question', QuestionController::class);

    Route::get('/question/create', [QuestionController::class, 'create'])
        ->name('question.create');
    Route::resource('/admin/question', QuestionController::class);

    Route::get('admin/animemovie/{animemovie}/delete', [AnimemovieController::class, 'delete'])
        ->name('animemovie.delete');
    Route::resource('/admin/animemovie', AnimemovieController::class);

    Route::get('/admin/animeserie/{animeserie}/delete', [AnimeserieController::class, 'delete'])
        ->name('animeserie.delete');
    Route::resource('/admin/animeserie', AnimeserieController::class);
});

require __DIR__ . '/auth.php';
