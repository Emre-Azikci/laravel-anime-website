<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimemoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animemovies', function (Blueprint $table) {
            $table->id();
            $table->string('moviename', 45);
            $table->string('summary', 200);
            $table->integer('releasedate');
            $table->foreignId('genre_id')->constrained()
                ->onUpdate('restrict')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animemovies');
    }
}
