<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimeseriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animeseries', function (Blueprint $table) {
            $table->id();
            $table->string('seriename', 45);
            $table->foreignId('season_id')->constrained()
            ->onUpdate('restrict')->onDelete('restrict');
            $table->text('summary');
            $table->foreignId('genre_id')->constrained()
            ->onUpdate('restrict')->onDelete('restrict');
            $table->bigInteger('releasedate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animeseries');
    }
}
