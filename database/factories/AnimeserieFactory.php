<?php

namespace Database\Factories;

use App\Models\Genre;
use App\Models\Season;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnimeserieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'seriename' => $this->faker->name,
            'season_id' => Season::all()->random()->id,
            'summary' => $this->faker->text,
            'genre_id' => Genre::all()->random()->id,
            'releasedate' => $this->faker->year()
        ];
    }
}
