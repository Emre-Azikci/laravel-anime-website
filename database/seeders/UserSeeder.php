<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Users role
        $user = User::factory()->create([
            'name' => 'User',
            'email' => 'user@opanime.com',
            'role_id' => 1,
            'password' => Hash::make('user')
        ]);
        $user->assignRole('user');

        // Admin role
        $admin = User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@opanime.com',
            'role_id' => 2,
            'password' => Hash::make('admin')
        ]);
        $admin->assignRole('admin');
    }
}
