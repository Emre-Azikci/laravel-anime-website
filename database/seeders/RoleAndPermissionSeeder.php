<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        //user
        Permission::create(['name' => 'index user']);
        Permission::create(['name' => 'show user']);
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'edit user']);
        Permission::create(['name' => 'delete user']);

        //Genre
        Permission::create(['name' => 'index genre']);
        Permission::create(['name' => 'show genre']);
        Permission::create(['name' => 'create genre']);
        Permission::create(['name' => 'edit genre']);
        Permission::create(['name' => 'delete genre']);

        //Question
        Permission::create(['name' => 'index question']);
        Permission::create(['name' => 'show question']);
        Permission::create(['name' => 'create question']);
        Permission::create(['name' => 'edit question']);
        Permission::create(['name' => 'delete question']);

        //Animeserie
        Permission::create(['name' => 'index animeserie']);
        Permission::create(['name' => 'show animeserie']);
        Permission::create(['name' => 'create animeserie']);
        Permission::create(['name' => 'edit animeserie']);
        Permission::create(['name' => 'delete animeserie']);

        //Animemovie
        Permission::create(['name' => 'index animemovie']);
        Permission::create(['name' => 'show animemovie']);
        Permission::create(['name' => 'create animemovie']);
        Permission::create(['name' => 'edit animemovie']);
        Permission::create(['name' => 'delete animemovie']);

        $user = Role::create(['name' => 'user'])
            ->givePermissionTo(['create question']);

        $admin = Role::create(['name' => 'admin'])
            ->givePermissionTo(Permission::all());
    }
}
