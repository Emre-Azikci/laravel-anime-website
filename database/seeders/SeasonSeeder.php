<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Season;

class SeasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $summer = Season::factory()->create([
            'seasonname' => 'Summer'
        ]);
        $fall = Season::factory()->create([
            'seasonname' => 'Fall'
        ]);

        $winter = Season::factory()->create([
            'seasonname' => 'Winter'
        ]);

        $spring = Season::factory()->create([
            'seasonname' => 'Spring'
        ]);
    }
}
