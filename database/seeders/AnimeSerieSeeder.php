<?php

namespace Database\Seeders;

use App\Models\Animeserie;
use Illuminate\Database\Seeder;

class AnimeSerieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $anime1 = Animeserie::factory()->create([
            'seriename' => 'One Piece'
        ]);

        $anime2 = Animeserie::factory()->create([
            'seriename' => 'Tensei shitara Slime Datta Ken'
        ]);

        $anime3 = Animeserie::factory()->create([
            'seriename' => 'Jujutsu Kaisen'
        ]);

        $anime4 = Animeserie::factory()->create([
            'seriename' => 'Hunter X Hunter'
        ]);
    }
}
