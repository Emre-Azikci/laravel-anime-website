<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Genre;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $action = Genre::factory()->create([
            'genrename' => 'Action'
        ]);
        $adventure = Genre::factory()->create([
            'genrename' => 'Adventure'
        ]);

        $comedy = Genre::factory()->create([
            'genrename' => 'Comedy'
        ]);

        $fantasy = Genre::factory()->create([
            'genrename' => 'Fantasy'
        ]);

        $drama = Genre::factory()->create([
            'genrename' => 'Drama'
        ]);

        $romance = Genre::factory()->create([
            'genrename' => 'Romance'
        ]);

        $school = Genre::factory()->create([
            'genrename' => 'School'
        ]);

        $superpower = Genre::factory()->create([
            'genrename' => 'Super Power'
        ]);

        $martialarts = Genre::factory()->create([
            'genrename' => 'Martial Arts'
        ]);

        $sliceoflife = Genre::factory()->create([
            'genrename' => 'Slice of Life'
        ]);
    }
}
