<?php

namespace Database\Seeders;

use App\Models\Animemovie;
use Illuminate\Database\Seeder;

class AnimemovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Movie1 = Animemovie::factory()->create([
            'moviename' => 'Tensei shitara Slime Datta Ken',
        ]);

        $Movie2 = Animemovie::factory()->create([
            'moviename' => 'One Piece',
        ]);

        $Movie3 = Animemovie::factory()->create([
            'moviename' => 'Hunter X Hunter',
        ]);

        $Movie4 = Animemovie::factory()->create([
            'moviename' => 'Jujutsu Kaisen',
        ]);
    }
}
