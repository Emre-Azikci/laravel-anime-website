@extends ('layouts.layout')

@section('content')

<div class="flex bg-purple-700 p-4 border">
    <h1 class="text-2xl font-semibold text-gray-700 dark:text-gray-200">Populair Movies</h1>
</div>

<div class="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4 gap-5 border">
    @foreach ($animemovies as $animemovie)
    <div class="rounded overflow-hidden">
        <a href="{{ route('open.animemovie.show', ['animemovie' => $animemovie->id]) }}">
            <img class="mx-auto transform transition duration-500 hover:scale-105" src="{{ asset('img/series/piece.jpg') }}" alt="One piece">
        </a>
        <div class="px-6 text-center py-4">
            <a class="font-bold text-sm mb-2 text-purple-700 hover:text-purple-800" href="{{ route('open.animemovie.show', ['animemovie' => $animemovie->id]) }}">
                {{$animemovie->moviename}}</a>
        </div>
        <div class="px-4 text-center pt-2 pb-2">
            <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Episode 1</span>
        </div>
    </div>
    @endforeach
</div>
@endsection