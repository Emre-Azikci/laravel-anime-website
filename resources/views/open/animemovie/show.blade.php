@extends ('layouts.layout')

@section('content')

<div class="flex bg-purple-700 p-4 border">
    <h1 class="text-2xl font-semibold text-gray-700 dark:text-gray-200">Anime Info</h1>
</div>

<div class="p-10 grid grid-cols-2 sm:grid-cols-2 md:grid-cols- lg:grid-cols- xl:grid-cols- gap-5 border">
    <div class="w-full">
        <img class="mx-auto float-left pr-4" src="{{ asset('img/series/piece.jpg') }}" alt="One piece">
        <h1 class="uppercase font-bold text-purple-700">{{$animemovie->moviename}}</h1>

        <p class="m-2 text-white text-sm">
            <span class="text-orange-600 text-sm font-bold">Summary: </span>
            {{$animemovie->summary}}
        </p>
        <p class="m-2">
            <span class="text-orange-600 text-sm font-bold">Genre: </span>
            <a class="text-white text-sm transform transition duration-500 hover:text-purple-700" href="#">{{$animemovie->genre->genrename}}</a>
        </p>
        <p class="m-2 text-white text-sm">
            <span class="text-orange-600 text-sm font-bold">Released: </span>
            {{$animemovie->releasedate}}
        </p>
    </div>
</div>

<div class="p-4 border">
    <h1 class="font-bold font-sans text-base text-white">Episodes</h1>
    <div>
        <ul class="flex mt-3">
            <li class="mr-3">
                <a class="inline-block border border-white rounded hover:border-gray-200 text-purple-700 hover:bg-gray-200 py-1 px-3" href="#">1</a>
            </li>
        </ul>
    </div>
</div>
@endsection