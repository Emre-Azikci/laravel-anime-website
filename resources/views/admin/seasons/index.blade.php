@extends ('layouts.layout')

@section('content')
    @if (session('status'))
        <div class="bg-green-200 text-green-900 rouned-lg shadow-md p-6 pr-10 mb-8">
            {{ session('status') }}
        </div>
    @endif

    <h3 class="text-gray-700 text-3xl font-medium">Seasons - Table</h3>

    <div class="flex flex-col mt-8">
        <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            <div
                class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200 dark:border-gray-800">
                <table class="w-full whitespace-no-wrap">
                    <thead>
                        <tr class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase border-b dark:border-gray-700 bg-gray-50 dark:text-gray-400 dark:bg-gray-800">
                            <th
                                class="px-4 py-3">
                                ID
                            </th>
                            <th
                                class="px-4 py-3">
                                Seasonname
                            </th>
                            <th
                                class="px-4 py-3">
                                Created_at
                            </th>
                            <th
                                class="px-4 py-3">
                                Updated_at
                            </th>
                            <th
                                class="px-4 py-3">
                                Edit
                            </th>
                            <th class="px-4 py-3">
                                Delete
                            </th>
                        </tr>
                    </thead>

                    <tbody class="bg-white divide-y dark:divide-gray-700 dark:bg-gray-800">
                        @foreach($seasons as $season)
                        <tr class="text-gray-700 dark:text-gray-400">
                            <!-- ID -->
                            <td class="px-4 py-3">
                                <div class="flex items-center text-sm">
                                    <p class="font-semibold">
                                        {{$season->id}}
                                    </p>
                                </div>
                            </td>
                            <!-- Name -->
                            <td class="px-4 py-3">
                                <div class="flex items-center text-sm">
                                    <p class="font-semibold">
                                        {{$season->seasonname}}
                                    </p>
                                </div>
                            </td>
                            <!-- Created at -->
                            <td class="px-4 py-3">
                                <div class="flex items-center text-sm">
                                    <p class="font-semibold">
                                        {{$season->created_at}}
                                    </p>
                                </div>
                            </td>
                            <!-- Updated at -->
                            <td class="px-4 py-3">
                                <div class="flex items-center text-sm">
                                    <p class="font-semibold">
                                        {{$season->updated_at}}
                                    </p>
                                </div>
                            </td>
                            <!-- Edit -->
                            <td class="px-4 py-3 text-sm">
                                <a href="{{ route('seasons.edit', ['season' => $season->id]) }}" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                            <!-- Delete -->
                            <td class="px-4 py-3 text-sm">
                                <a href="{{ route('seasons.delete', ['season' => $season->id]) }}" class="text-indigo-600 hover:text-indigo-900">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="text-right mt-2">
                <a href="{{ route('seasons.create') }}" class="mr-4 text-orange-600 hover:text-orange-900 font-bold">Add +</a>
            </div>
        </div>
    </div>
@endsection
