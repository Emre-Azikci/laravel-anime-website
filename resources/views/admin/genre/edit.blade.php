@extends ('layouts.layout')

@section('content')
@if ($errors->any())
<div class="bg-red-200 text-red-900 rounded-lg shadow-md p-6 pr-10 mb-8 ">
    <ul class="mt-3 list-disc list-inside text-sn text-red-600">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="bg-white p-10 md:w-3/4 lg:w-1/2 mx-auto rounded-md">
    <form action="{{route('genre.update', ['genre' => $genre->id]) }}" method="POST">
        @method('PUT')
        @csrf
        <!-- Genrename -->
        <div class="flex flex-col mb-5">
            <label for="genrename" class="inline-block w-30 mr-6 text-left 
                                    font-bold text-gray-600">Genrename</label>
            <input type="text" required id="genrename" name="genrename" placeholder="Enter genrename" value="{{ old('genrename', $genre->genrename)}}" 
                            class="flex-1 py-2 border-b-2 focus:border-indigo-600 text-gray-600 placeholder-gray-400 
                            rounded-md @error('genrename') border-red-500 @enderror
                            outline-none">
        </div>

        <!-- Buttons -->
        <div class="text-right">
            <button class="py-3 px-8 bg-indigo-600 text-white font-bold rounded-md">Edit</button>
        </div>
        <div class="text-right mt-2">
            <a href="{{route('genre.index')}}" class="mr-6 text-sm text-red-600 font-bold">Abort</a>
        </div>
    </form>
</div>
@endsection