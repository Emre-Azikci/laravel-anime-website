@extends ('layouts.layout')

@section('content')
<div class="bg-white p-10 md:w-3/4 lg:w-1/2 mx-auto rounded-md">
    <form action="{{ route('genre.destroy', ['genre' => $genre->id]) }}" method="POST">
        @method('DELETE')
        @csrf
        <div class="flex flex-col mb-5">
            <!-- Genrename -->
            <label for="genrename" class="inline-block w-30 mr-6 text-left 
                                    font-bold text-gray-600">Genrename</label>
            <input type="text" disabled id="genrename" name="genrename" value="{{ $genre->genrename }}" 
                            class="flex-1 py-2 border-b-2 border-gray-400 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md
                            outline-none cursor-not-allowed">
            </div>

        <!-- Buttons -->
        <div class="text-right">
            <button class="py-3 px-8 bg-indigo-600 text-white font-bold rounded-md">Delete</button>
        </div>
        <div class="text-right mt-2">
            <a href="{{route('genre.index')}}" class="mr-8 text-sm text-red-600 font-bold">Abort</a>
        </div>
    </form>
</div>
@endsection
