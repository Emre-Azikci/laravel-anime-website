@extends ('layouts.layout')

@section('content')
    @if ($errors->any())
        <div class="bg-red-200 text-red-900 rounded-lg shadow-md p-6 pr-10 mb-8 ">
            <ul class="mt-3 list-disc list-inside text-sn text-red-600">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="bg-white p-10 md:w-3/4 lg:w-1/2 mx-auto rounded-md">
        <form action="{{ route('animeserie.update', ['animeserie' => $animeserie->id]) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="flex flex-col mb-5">
                <!-- Seriename -->
                <label for="seriename" class="inline-block w-30 mr-6 text-left 
                            font-bold text-gray-600">Animeserie</label>
                <input type="text" required id="seriename" name="seriename" placeholder="Enter Animeserie" value="{{ old('animeserie', $animeserie->seriename)}}"
                            class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md @error('seriename') border-red-500 @enderror
                            outline-none mb-2">

                <!-- Seasonname -->
                <label for="seasonname" class="inline-block w-30 mr-6 text-left 
                            font-bold text-gray-600">Seasonname</label>
                <select type="text" required id="seasonname" name="season_id"
                        class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                        text-gray-600 placeholder-gray-400 rounded-md outline-none mb-2">
                    @foreach ($seasons as $season)
                        <option value="{{$season->id}}">{{$season->seasonname}}</option>
                    @endforeach
                </select>

                <!-- Summary -->
                <label for="summary" class="inline-block w-30 mr-6 text-left 
                            font-bold text-gray-600">Summary</label>
                <textarea id="summary" required name="summary" placeholder="Add summary"
                            class="py-2 border focus:border-indigo-600 
                            text-gray-600 rounded-md placeholder-gray-400 @error('summary') border-red-500 @enderror
                            outline-none mb-2">{{$animeserie->summary}}</textarea>

                <!-- Genrename -->
                <label for="genrename" class="inline-block w-30 mr-6 text-left 
                            font-bold text-gray-600">Genrename</label>
                <select type="text" required id="genrename" name="genre_id"
                        class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                        text-gray-600 placeholder-gray-400 rounded-md outline-none mb-2">
                    @foreach ($genres as $genre)
                        <option value="{{$genre->id}}">{{$genre->genrename}}</option>
                    @endforeach
                </select>

                <!-- Releasedate -->
                <div class="custom-number-input h-auto w-32">
                    <label for="releasedate" class="inline-block w-30 mr-6 text-left 
                        font-bold text-gray-600">Releasedate(Year)</label>
                    <div class="flex flex-row h-10 w-full rounded-lg relative bg-transparent mt-1">
                        <input type="number" required class="outline-none rounded focus:outline-none text-center w-full bg-gray-300 font-semibold
                         text-md hover:text-black focus:text-black  md:text-basecursor-default flex items-center text-gray-700" 
                         id="releasedate" name="releasedate" value="{{ old('animeserie', $animeserie->releasedate)}}">
                    </div>     
                </div>

                <!-- Buttons -->
                <div class="text-right">
                    <button class="py-3 px-8 bg-indigo-600 text-white font-bold rounded-md">Edit</button>
                </div>
                <div class="text-right mt-2">
                    <a href="{{route('animeserie.index')}}" class="mr-8 text-sm text-red-600 font-bold">Abort</a>
                </div>
            </div>
        </form>
    </div>
@endsection