@extends ('layouts.layout')

@section('content')
    <div class="bg-white p-10 md:w-3/4 lg:w-1/2 mx-auto rounded-md">
        <form action="{{ route('animeserie.destroy', ['animeserie' => $animeserie->id]) }}" method="POST">
            @method('DELETE')
            @csrf
            <div class="flex flex-col mb-5">
                <!-- Seriename -->
                <label for="seriename" class="inline-block w-30 mr-6 text-left 
                            font-bold text-gray-600">Animeserie</label>
                <input type="text" disabled id="seriename" name="seriename" value="{{$animeserie->seriename}}"
                            class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md @error('seriename') border-red-500 @enderror
                            outline-none mb-2 cursor-not-allowed">

                <!-- Seasonname -->
                <label for="seasonname" class="inline-block w-30 mr-6 text-left 
                            font-bold text-gray-600">Seasonname</label>
                <input type="text" disabled id="seasonname" name="seasonname" value="{{$animeserie->season->seasonname}}"
                            class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md @error('seasonname') border-red-500 @enderror
                            outline-none mb-2 cursor-not-allowed">

                <!-- Summary -->
                <label for="summary" class="inline-block w-30 mr-6 text-left 
                            font-bold text-gray-600">Summary</label>
                <textarea disabled id="summary" name="summary"
                            class="py-2 border focus:border-indigo-600 
                            text-gray-600 rounded-md placeholder-gray-400 @error('summary') border-red-500 @enderror
                            outline-none mb-2 cursor-not-allowed">{{$animeserie->summary}}</textarea>

                <!-- Seriename -->
                <label for="genrename" class="inline-block w-30 mr-6 text-left 
                            font-bold text-gray-600">Genrename</label>
                <input type="text" disabled id="genrename" name="genrename" value="{{$animeserie->genre->genrename}}"
                            class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md @error('genrename') border-red-500 @enderror
                            outline-none mb-2 cursor-not-allowed">

                <!-- Releasedate -->
                <div class="custom-number-input h-auto w-32">
                    <label for="releasedate" class="inline-block w-30 mr-6 text-left 
                        font-bold text-gray-600">Releasedate(Year)</label>
                    <div class="flex flex-row h-10 w-full rounded-lg relative bg-transparent mt-1">
                        <input type="number" disabled class="outline-none rounded focus:outline-none text-center w-full bg-gray-300 font-semibold
                         text-md hover:text-black focus:text-black  md:text-basecursor-default flex items-center text-gray-700 cursor-not-allowed" 
                         id="releasedate" name="releasedate" value="{{$animeserie->releasedate}}">
                    </div>     
                </div>
    
                <!-- Buttons -->
                <div class="text-right">
                    <button class="py-3 px-8 bg-indigo-600 text-white font-bold rounded-md">Delete</button> 
                </div>
                <div class="text-right mt-2">
                    <a href="{{route('animeserie.index')}}" class="mr-8 text-sm text-red-600 font-bold">Abort</a>
                </div>
            </div>
        </form>
    </div>
@endsection