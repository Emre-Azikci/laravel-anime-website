@extends ('layouts.layout')

@section('content')
<div class="bg-white p-10 md:w-3/4 lg:w-1/2 mx-auto rounded-md">
    <form action="{{ route('animemovie.destroy', ['animemovie' => $animemovie->id]) }}" method="POST">
        @method('DELETE')
        @csrf
        <!-- Name input -->
        <div class="flex flex-col mb-5">
            <label for="moviename" class="inline-block w-30 mr-6 text-left 
                                    font-bold text-gray-600">Animemovie</label>
            <input type="text" disabled id="moviename" name="moviename" placeholder="Moviename" value="{{ $animemovie->moviename }}" class="flex-1 py-2 border-b-2 border-gray-400 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md
                            outline-none">
        </div>

        <div class="text-right">
            <button class="py-3 px-8 bg-indigo-600 text-white font-bold rounded-md">Delete</button>
        </div>
        <div class="text-right mt-2">
            <a href="{{route('animemovie.index')}}" class="mr-8 text-sm text-red-600 font-bold">Abort</a>
        </div>
    </form>
</div>
@endsection