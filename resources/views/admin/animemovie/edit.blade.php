@extends ('layouts.layout')

@section('content')
@if ($errors->any())
<div class="bg-red-200 text-red-900 rounded-lg shadow-md p-6 pr-10 mb-8 ">
    <ul class="mt-3 list-disc list-inside text-sn text-red-600">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="bg-white p-10 md:w-3/4 lg:w-1/2 mx-auto rounded-md">
    <form action="{{route('animemovie.update', ['animemovie' => $animemovie->id]) }}" method="POST">
        @method('PUT')
        @csrf
        <!-- Name input -->
        <div class="flex flex-col mb-5">
            <label for="moviename" class="inline-block w-30 mr-6 text-left 
                                    font-bold text-gray-600">Animemovie</label>
            <input type="text" required id="moviename" name="moviename" placeholder="Moviename" value="{{ old('moviename', $animemovie->moviename)}}" class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md @error('moviename') border-red-500 @enderror
                            outline-none">
        </div>

        <!-- Summary input -->
        <div class="flex flex-col mb-5">
            <label for="summary" class="inline-block w-30 mr-6 text-left 
                                    font-bold text-gray-600">Summary</label>
            <input type="text" required id="summary" name="summary" placeholder="Summary" value="{{ old('summary', $animemovie->summary)}}" class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md @error('summary') border-red-500 @enderror
                            outline-none">
        </div>

        <!-- Releasedate input -->
        <div class="flex flex-col mb-5">
            <label for="releasedate" class="inline-block w-30 mr-6 text-left 
                                    font-bold text-gray-600">Releasedate</label>
            <input type="number" required id="releasedate" name="releasedate" placeholder="Releasedate" value="{{ old('releasedate', $animemovie->releasedate)}}" class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md @error('releasedate') border-red-500 @enderror
                            outline-none">
        </div>        

        <!-- Genre input -->
        <div class="flex flex-col mb-5">
            <label for="genrename" class="inline-block w-30 mr-6 text-left font-bold text-gray-600">Genrename</label>
            <select type="text" required id="genrename" name="genre_id" class="flex-1 py-2 border-b-2 focus:border-indigo-600 text-gray-600 placeholder-gray-400 rounded-md outline-none">
                @foreach ($genres as $genre)
                <option value="{{$genre->id}}">{{$genre->genrename}}</option>
                @endforeach
            </select>
        </div>     

        <div class="text-right">
            <button class="py-3 px-8 bg-indigo-600 text-white font-bold rounded-md">Edit</button>
        </div>
        <div class="text-right mt-2">
            <a href="{{route('animemovie.index')}}" class="mr-6 text-sm text-red-600 font-bold">Abort</a>
        </div>
    </form>
</div>
@endsection