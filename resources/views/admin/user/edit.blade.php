@extends ('layouts.layout')

@section('content')
@if ($errors->any())
<div class="bg-red-200 text-red-900 rounded-lg shadow-md p-6 pr-10 mb-8 ">
    <ul class="mt-3 list-disc list-inside text-sn text-red-600">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="bg-white p-10 md:w-3/4 lg:w-1/2 mx-auto rounded-md">
    <form action="{{route('user.update', ['user' => $user->id]) }}" method="POST">
        @method('PUT')
        @csrf
        <!-- Name input -->
        <div class="flex flex-col mb-5">
            <label for="name" class="inline-block w-30 mr-6 text-left 
                                    font-bold text-gray-600">User</label>
            <input type="text" required id="name" name="name" placeholder="Name" value="{{ old('name', $user->name)}}" class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md @error('name') border-red-500 @enderror
                            outline-none">
        </div>
        <!-- Email input -->
        <div class="flex flex-col mb-5">
            <label for="email" class="inline-block w-30 mr-6 text-left 
                                    font-bold text-gray-600">Email</label>
            <input type="text" required id="email" name="email" placeholder="Email" value="{{ old('email', $user->email)}}" class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md @error('email') border-red-500 @enderror
                            outline-none">
        </div>
        <!-- Password input -->
        <div class="flex flex-col mb-5">
            <label for="password" class="inline-block w-30 mr-6 text-left 
                                    font-bold text-gray-600">Password</label>
            <input type="text" required id="password" name="password" placeholder="{{ old('password', $user->password)}}" class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                            text-gray-600 placeholder-gray-400 rounded-md @error('password') border-red-500 @enderror
                            outline-none ">
        </div>

        <div class="text-right">
            <button class="py-3 px-8 bg-indigo-600 text-white font-bold rounded-md">Edit</button>
        </div>
        <div class="text-right mt-2">
            <a href="{{route('user.index')}}" class="mr-6 text-sm text-red-600 font-bold">Abort</a>
        </div>
    </form>
</div>
@endsection