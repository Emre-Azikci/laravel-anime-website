@extends ('layouts.layout')

@section('content')
@if (session('status'))
<div class="bg-green-200 text-green-900 rouned-lg shadow-md p-6 pr-10 mb-8">
    {{ session('status') }}
</div>
@endif

<h3 class="text-2xl font-semibold text-gray-700 dark:text-gray-200">Question - Table</h3>

<div class="flex flex-col mt-8">
    <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200 dark:border-gray-800">
            <table class="w-full whitespace-no-wrap">
                <thead>
                    <tr class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase border-b dark:border-gray-700 bg-gray-50 dark:text-gray-400 dark:bg-gray-800">
                        <th class="px-4 py-3">
                            ID
                        </th>
                        <th class="px-4 py-3">
                            User
                        </th>
                        <th class="px-4 py-3">
                            animetitle
                        </th>
                        <th class="px-4 py-3">
                            Genrename
                        </th>
                        <th class="px-4 py-3">
                            Created_at
                        </th>
                        <th class="px-4 py-3">
                            Updated_at
                        </th>
                        <th class="px-4 py-3">
                            Show
                        </th>
                        <th class="px-4 py-3">
                            Delete
                        </th>
                    </tr>
                </thead>

                <tbody class="bg-white divide-y dark:divide-gray-700 dark:bg-gray-800">
                    @foreach($questions as $question)
                    <tr class="text-gray-700 dark:text-gray-400">
                        <!-- ID -->
                        <td class="px-4 py-3">
                            <div class="flex items-center text-sm">
                                <p class="font-semibold">
                                    {{$question->id}}
                                </p>
                            </div>
                        </td>
                        <!-- User -->
                        <td class="px-4 py-3">
                            <div class="flex items-center text-sm">
                                <p class="font-semibold">
                                    {{$question->user->name}}
                                </p>
                            </div>
                        </td>
                        <!-- Animetitle -->
                        <td class="px-4 py-3">
                            <div class="flex items-center text-sm">
                                <p class="font-semibold">
                                    {{$question->animetitle}}
                                </p>
                            </div>
                        </td>
                        <!-- Genrename -->
                        <td class="px-4 py-3">
                            <div class="flex items-center text-sm">
                                <p class="font-semibold">
                                    {{$question->genre->genrename}}
                                </p>
                            </div>
                        </td>
                        <!-- Created at -->
                        <td class="px-4 py-3">
                            <div class="flex items-center text-sm">
                                <p class="font-semibold">
                                    {{$question->created_at}}
                                </p>
                            </div>
                        </td>
                        <!-- Updated at -->
                        <td class="px-4 py-3">
                            <div class="flex items-center text-sm">
                                <p class="font-semibold">
                                    {{$question->updated_at}}
                                </p>
                            </div>
                        </td>
                        <!-- Edit -->
                        <td class="px-4 py-3 text-sm">
                            <a href="{{ route('question.show', ['question' => $question->id]) }}" class="text-indigo-600 hover:text-indigo-900">Show</a>
                        <!-- Delete -->
                        <td class="px-4 py-3 text-sm">
                            <a href="{{ route('question.delete', ['question' => $question->id]) }}" class="text-indigo-600 hover:text-indigo-900">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="text-right mt-2">
            <a href="{{route('question.create')}}" class="mr-4 text-orange-600 hover:text-orange-900 font-bold">Add +</a>
        </div>
    </div>
</div>
@endsection