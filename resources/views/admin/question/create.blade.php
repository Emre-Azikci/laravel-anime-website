@extends ('layouts.layout')

@section('content')
@if ($errors->any())
<div class="bg-red-200 text-red-900 rounded-lg shadow-md p-6 pr-10 mb-8 ">
    <ul class="mt-3 list-disc list-inside text-sn text-red-600">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (session('status'))
<div class="bg-green-200 text-green-900 rouned-lg shadow-md p-6 pr-10 mb-8">
    {{ session('status') }}
</div>
@endif

<div class="bg-white p-10 md:w-3/4 lg:w-1/2 mx-auto rounded-md">
    <form action="{{route('question.store')}}" method="POST">
        @csrf
        <div class="flex flex-col mb-5">
            <div class="flex flex-col mb-5">
                <!-- User -->
                <label for="name" class="inline-block w-30 mr-6 text-left 
                                font-bold text-gray-600">User</label>
                <input type="text" disabled id="name" name="name" value="{{ Auth::user()->name }}"
                                class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                                text-gray-400 rounded-md outline-none cursor-not-allowed mb-2">

                <!-- Animetitle -->
                <label for="animetitle" class="inline-block w-30 mr-6 text-left 
                                font-bold text-gray-600">Animetitle</label>
                <input type="text" required id="animetitle" name="animetitle" placeholder="Enter animetitle" value="{{ old('animetitle')}}" 
                                class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                                text-gray-600 placeholder-gray-400 rounded-md @error('animetitle') border-red-500 @enderror
                                outline-none mb-2">

                <!-- Genrename -->
                <label for="genrename" class="inline-block w-30 mr-6 text-left 
                                font-bold text-gray-600">Genrename</label>
                <select type="text" required id="genrename" name="genrename" class="flex-1 py-2 border-b-2 focus:border-indigo-600 
                    text-gray-600 placeholder-gray-400 rounded-md outline-none">
                        @foreach ($genres as $genre)
                            <option value="{{$genre->id}}">{{$genre->genrename}}</option>
                        @endforeach
                    </select>
                </div>
    
                <!-- Buttons -->
                <div class="text-right">
                    <button class="py-3 px-8 bg-indigo-600 text-white font-bold rounded-md">Submit</button> 
                </div>
                <div class="text-right mt-2">
                    <a href="{{route('open.animeserie.index')}}" class="mr-10 text-sm text-red-600 font-bold">Abort</a>
                </div>
            </div>
        </form>
    </div>
@endsection