@extends ('layouts.layout')



@section('content')
<h3 class="text-2xl font-semibold text-gray-700 dark:text-gray-200">Overview</h3>

<div class="flex flex-col mt-8">
    <div class="py-12 bg-gray-300 w-4/5 m-auto border-2 rounded border-purple-600">
        <div class="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-md">
            <div class="md:flex">
                <div class="w-full p-2 py-10">
                    <div class="flex justify-center">
                        <div class="relative">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThbX6_TY4bdz5obxfFa4OsjcGkxFEwAUBsiw&usqp=CAU" class="rounded-full" width="80">
                        </div>
                    </div>
                    <div class="flex flex-col text-center mt-3 mb-4">
                        <span class="text-2xl font-medium">{{$question->user->name}}</span>
                        <span class="text-md text-gray-400">UserID: {{$question->user->id}}</span>
                        <span class="text-md text-gray-400">QuestionID: {{$question->id}}</span>
                    </div>
                    <div class="border border-orange-600 p-1 mt-3 text-center">
                        <p class="px-16 text-center text-md text-gray-800">Requested animetitle: {{$question->animetitle}}</p>
                        <span>Genrename:</span>
                        <span class="bg-gray-100 h-5 p-1 px-3 rounded cursor-pointer hover:shadow hover:bg-gray-200">{{$question->genre->genrename}}</span>
                        <p class="px-16 text-center text-md text-gray-800">Date: {{$question->created_at}}</p>

                    </div>
                    <div class="px-14 mt-5">
                        <button class="h-12 bg-gray-200 w-full text-black text-md rounded hover:shadow hover:bg-red-300 mb-2">Delete</button>
                        <button class="h-12 bg-blue-700 w-full text-white text-md rounded hover:shadow hover:bg-green-800">Add</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection