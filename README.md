# Laravel Anime Website

An anime website made with Laravel.

I've made the CRUDs for the Anime movie and Users/Permissions, I've also made most of the layout and the admin dashboard.

All the tests for the CRUDs above were made by me.

This was a school project made together with Yusuf Koc.
